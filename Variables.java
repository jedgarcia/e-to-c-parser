import java.util.*;

class Variables{
	public String name;
	public int scopelevel;
	public int line;

	public Variables (){};

	public Variables(String name, int scopelevel, int line){
		this.name = name;
		this.scopelevel = scopelevel;
		this.line = line;
	}

	public String getname(){
		return name;
	}

	public int getlevel(){
		return scopelevel;
	}

	public int getline(){
		return line;
	}


	//debugging main function which will be used to test Variables class, explicitly tell
	//what values to use

	/*public static void main(String args[]) {
 		
 		Variables test = new Variables("x", 1 , 3);

 		System.out.println(test.line);

    }*/
}