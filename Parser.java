/* *** This file is given as part of the programming assignment. *** */

public class Parser {


    // tok is global to all these parsing methods;
    // scan just calls the scanner's scan method and saves the result in tok.
    private Token tok; // the current token

    First first = new First();        //create a new instance of First()
    Symboltable symbols = new Symboltable(); //create a new instance of a symbol table
    private String forvariable;
    private int forvariablelevel;

    private void scan() {
        tok = scanner.scan();
    }

    private Scan scanner;
    Parser(Scan scanner) {
        this.scanner = scanner;
        scan();
        program();
        if( tok.kind != TK.EOF ){
            parse_error("junk after logical end of program");
        }
    }

    private void program() {
    	gen_C("#include <stdio.h>");
    	gen_C("void main()");
        block();
    }

    private void block(){
    	gen_C("{");
        symbols.add_scope_level();
        declaration_list();
        statement_list();
        symbols.remove_scope_level();
        gen_C("}");
    }

    private void declaration_list() {
        // below checks whether tok is in first set of declaration.
        while( is(TK.DECLARE) ) {
            declaration();
        }
    }

    private void declaration() {
        mustbe(TK.DECLARE);
        if(is(TK.ID)){
            if(!symbols.id_in_currentlevel(tok.string)){
                symbols.add_variable_to_block(tok.string, symbols.level, tok.lineNumber);       //add variable to currrent table level if not declared
                gen_C("int "+ tok.string + "_" + Integer.toString(symbols.getlevel(tok.string)) + ";");
            }
            else{
                System.err.println("redeclaration of variable "+ tok.string);
            }
            scan();
        }
        while( is(TK.COMMA) ) {
            scan();
            if(is(TK.ID)){
                if( symbols.id_in_currentlevel(tok.string) ){
                    System.err.println("redeclaration of variable "+ tok.string);
                }
                else{
                    symbols.add_variable_to_block(tok.string, symbols.level, tok.lineNumber);       //add variable to currrent table level if not declared
                    gen_C("int "+ tok.string + "_" + Integer.toString(symbols.getlevel(tok.string))+ ";");
                }
                scan();
            }
        }
    }
    // NEW Functions for syntax checking///////////////////////////////////////////////////////////////////
    private void statement_list() {
        while(First.Check("STATEMENT_LIST", tok.kind.toString())){
            statement();
        }
    }
    
    private void statement(){
        if(First.Check("ASSIGNMENT", tok.kind.toString())){
            assignment();
        }
        else if(is(TK.PRINT)){
            print();
        }
        else if(is(TK.DO)){
            e_do();
        }
        else if(is(TK.IF)){
            e_if();
        }
        else if(is(TK.FOR)){
            e_for();
        }
    }

    private void print(){
        mustbe(TK.PRINT);
        gen_C("printf(\"%d\\n\",");
        expression();
        gen_C(");");
    }

    private Variables assignment(){
        Variables holding = new Variables();

        if(First.Check("REF_ID",tok.kind.toString())){
            holding=ref_id();
        }
        else{
            parse_error("invalid ID value");
        }
        mustbe(TK.ASSIGN);
        gen_C("=");
        expression();
        gen_C(";");
        return holding;
    }

    private Variables ref_id(){
        Variables holder2 = new Variables ();
        if(is(TK.TILDE)){
            scan();
            if(is(TK.NUM)){
                int scopecheck = Integer.parseInt(tok.string);
                scan();
                if(is(TK.ID)){
                    if(scopecheck == 0){
                        if( symbols.id_in_currentlevel(tok.string)){
                            gen_C(tok.string + "_" + Integer.toString(symbols.getlevel(tok.string)));
                            holder2=new Variables(tok.string, (symbols.level - scopecheck), tok.lineNumber);
                            scan();
                            return (holder2);  //THIS HAD BEEN ADDED
                        }
                        else{
                            System.err.println("no such variable ~"+ scopecheck + tok.string + " on line " + tok.lineNumber);
                            System.exit(1);
                        }
                    }
                    else{
                        if( symbols.id_in_level(tok.string,scopecheck)){
                        	gen_C(tok.string + "_" + Integer.toString(symbols.level - scopecheck));
                            holder2=new Variables(tok.string, (symbols.level - scopecheck), tok.lineNumber);
                            scan();
                            return (holder2);  //THIS HAD BEEN ADDED
                        }
                        else{
                            System.err.println("no such variable ~"+ scopecheck + tok.string + " on line " + tok.lineNumber);
                            System.exit(1);
                        }
                    }
                }
            }
            else{
                if(is(TK.ID)){
                    if( symbols.id_in_global(tok.string)){
                        gen_C(tok.string+"_"+"1");
                        holder2=new Variables(tok.string, symbols.getlevel(tok.string), tok.lineNumber);
                        scan();  
                        return (holder2);  //THIS HAD BEEN ADDED 
                    }
                    else{
                        System.err.println("no such variable ~" + tok.string + " on line " + tok.lineNumber);
                        System.exit(1);
                    }
                }
            }
        }
        else{
            if( is(TK.ID) ){
                if( symbols.id_in_scope(tok.string) ){
                    gen_C(tok.string +"_"+Integer.toString(symbols.getlevel(tok.string)));
                    holder2=new Variables(tok.string, symbols.getlevel(tok.string), tok.lineNumber);
                    scan();
                    return (holder2);  //THIS HAD BEEN ADDED
                }
                else{
                    System.err.println(tok.string + " is an undeclared variable on line " + tok.lineNumber);
                    System.exit(1);
                }
            }
        }
        return holder2;
    }

    private void e_do(){
        mustbe(TK.DO);
        gen_C("while(");
        guarded_command();
        mustbe(TK.ENDDO);
    }

    private void e_if(){
        mustbe(TK.IF);
        gen_C("if(");
        guarded_command();
        while(is(TK.ELSEIF)){
            scan();
            gen_C("else if(");
            guarded_command();
        }
        if(is(TK.ELSE)){
        	gen_C("else");
            scan();
            block();
        }
        mustbe(TK.ENDIF);
    }

    private void guarded_command(){
    	expression();
        gen_C(" < 1");            // guarded commands execute when the comparison is nonpositive
        mustbe(TK.THEN);
        gen_C(")");
        block();
    }

    private void expression(){
        term();
        while(First.Check("ADDOP", tok.kind.toString())){
            addop();
            term();
        }
    }

    private void term(){
        factor();
        while(First.Check("MULTOP", tok.kind.toString())){
            multop();
            factor();
        }
    }

    private void factor(){
        if(is(TK.LPAREN)){
            scan();
            gen_C("(");
            expression();
            mustbe(TK.RPAREN);
            gen_C(")");
        }
        else if(First.Check("REF_ID", tok.kind.toString())){
            ref_id();
        }
        else if(is(TK.NUM)){
        	gen_C(tok.string);
            scan();
        }
        else{
            parse_error("invalid factor in expression");
        }
    }

    private void addop(){
        if(is(TK.PLUS)){
        	scan();
            gen_C("+");
        }
        else if(is(TK.MINUS)){
        	scan();
            gen_C("-");
        }
        else{
            parse_error("invalid addition operator");
        }
    }


    private void multop(){
        if(is(TK.TIMES)){
        	scan();
            gen_C("*");
        }
        else if(is(TK.DIVIDE)){
        	scan();
            gen_C("/");
        }
        else{
            parse_error("invalid mult operator");
        }
    }
    private void e_for(){
        mustbe(TK.FOR);
        mustbe(TK.LPAREN);
        gen_C("for(");

         Variables forloop =new Variables();
         forloop= assignment();

        mustbe(TK.THEN);
        if(is(TK.PLUS)){
            gen_C(forloop.name+"_"+forloop.scopelevel + " < ");
            scan();
            expression();
            gen_C(";");
            if(is(TK.NUM)){
                gen_C(forloop.name+"_"+forloop.scopelevel + " +=" + Integer.parseInt(tok.string) + ")");
                scan();
                mustbe(TK.RPAREN);
            }
            else{
                gen_C(forloop.name+"_"+forloop.scopelevel +"++"+")");
                scan();            }
        }
        else{
            gen_C(forloop.name+"_"+forloop.scopelevel + " > ");
            scan();
            expression();
            gen_C(";");
            if(is(TK.NUM)){
                gen_C(forloop.name+"_"+forloop.scopelevel + " -=" + Integer.parseInt(tok.string) + ")");
                scan();
                mustbe(TK.RPAREN);
            }
            else{
                gen_C(forloop.name+"_"+forloop.scopelevel +"--"+")");
                scan();
            }
        }
        block();
    }

    //Checks to see if the current token is
    //what we want; also, it is used for singleton 
    //first sets.
    private boolean is(TK tk) {
        return tk == tok.kind;
    }

    // ensure current token is tk and skip over it.
    private void mustbe(TK tk) {
        if( ! is(tk) ) {
            System.err.println( "mustbe: want " + tk + ", got " +
                                    tok);
            parse_error( "missing token (mustbe)" );
        }
        scan();
    }

    private void parse_error(String msg) {
        System.err.println( "can't parse: line "
                            + tok.lineNumber + " " + msg );
        System.exit(1);
    }
    
    private void gen_C(String msg) {
    	System.out.println(msg);
    }
    
}
