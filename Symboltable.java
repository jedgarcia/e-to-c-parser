import java.util.*;

public class Symboltable {

	//We create a stack of array lists each containing its own set of
	//variables
	private Stack<ArrayList<Variables>> codeblock;
	public int level;

	//constructor for symbol table
	public Symboltable(){
		codeblock = new Stack<ArrayList<Variables>>();
		level = 0;                //set to zero so that we can use the list iterator function previous(); will read
								  //as level-1, since stack starts at 0.
	}

	//signifies the beginning of a code block
	public void add_scope_level(){
		ArrayList<Variables> scope = new ArrayList<Variables>();
		codeblock.push(scope);
		level++;
	}

	//signifies the end of a codeblock
	public void remove_scope_level(){
		codeblock.pop();
		level--;
	}

	//check the current level and see if the variable can be found in there
	public boolean id_in_currentlevel(String desiredvariable){
		ArrayList<Variables> currentlevel = codeblock.peek();  //creates an array list with the current block variables
		for(int i = 0; i < currentlevel.size(); i++){         // iterate through the variables array
			if( currentlevel.get(i).name.equals(desiredvariable)){   //check and see if the variable has been declared
				return true;
			}
		}
		return false;
	}

	//check the scope of the current level and see if the variable is in there
	public boolean id_in_scope(String desiredvariable){
		// we must start at the top level of the scope and then go outside it as we search, therefore we must
		// search the stack from top to bottom, we do not want to pop() until we finish the block
		// initiallizes list iterator to the top of the stack
		// we can use the previous function to traverse the stack
		// until there are no more tables to look 
		ListIterator<ArrayList<Variables>> checkblock = codeblock.listIterator(codeblock.size());
		while( checkblock.hasPrevious() ){
			ArrayList<Variables> currentlevel = checkblock.previous();
			for(int i = 0; i < currentlevel.size(); i++){
				if(currentlevel.get(i).name.equals(desiredvariable)){
					return true;
				}
			}
		}

		return false;

	}

	public int getlevel(String desiredvariable){
		// we must start at the top level of the scope and then go outside it as we search, therefore we must
		// search the stack from top to bottom, we do not want to pop() until we finish the block
		// initiallizes list iterator to the top of the stack
		// we can use the previous function to traverse the stack
		// until there are no more tables to look 
		ListIterator<ArrayList<Variables>> checkblock = codeblock.listIterator(codeblock.size());
		while( checkblock.hasPrevious() ){
			ArrayList<Variables> currentlevel = checkblock.previous();
			for(int i = 0; i < currentlevel.size(); i++){
				if(currentlevel.get(i).name.equals(desiredvariable)){
					return currentlevel.get(i).scopelevel;
				}
			}
		}

		return 1;
	}

	//add a variable into the current scope's level
	public void add_variable_to_block(String varname, int scope_level, int linenum){
		boolean exists = id_in_currentlevel(varname);                   // will return false by default if not already in symbol table
		if(exists){
			System.err.println("redeclaration of variable " + varname);
		}
		else{
			Variables temp = new Variables(varname, scope_level, linenum);
			codeblock.peek().add(temp);
		}
	}

	public void tableprint(){
		ListIterator<ArrayList<Variables>> checkblock = codeblock.listIterator(codeblock.size());
		while( checkblock.hasPrevious() ){
			ArrayList<Variables> check = checkblock.previous();
			for(int i = 0; i < check.size(); i++){
				System.out.println(check.get(i).name + " " + check.get(i).scopelevel + " ");
			}
			System.out.println("/////////////////////////\n");
		}
	}

	public boolean id_in_level(String desiredvariable, int user_level){
		if(user_level < level){
		ListIterator<ArrayList<Variables>> checkblock = codeblock.listIterator(level - user_level); //creates an array list with a specific block
			if(checkblock.hasPrevious()){
			ArrayList<Variables> currentlevel = checkblock.previous();    //retieves the array list of the specific level
			for(int i = 0; i < currentlevel.size(); i++){        		  //iterate through the variables array
				if( currentlevel.get(i).name.equals(desiredvariable)){    //check and see if the variable has been declared
					return true;
					}
				}
				return false;
			}
		} 
		return false;					
	}

	public boolean id_in_global(String desiredvariable){
		ListIterator<ArrayList<Variables>> checkblock = codeblock.listIterator(1); //creates an array list with a specific block
			if(checkblock.hasPrevious()){
			ArrayList<Variables> currentlevel = checkblock.previous();    //retieves the array list of the specific level
			for(int i = 0; i < currentlevel.size(); i++){        		  //iterate through the variables array
				if( currentlevel.get(i).name.equals(desiredvariable)){    //check and see if the variable has been declared
					return true;
					}
				}
				return false;
			}
		return false;					
	}


/*	public static void main(String args[]){
	Symboltable table = new Symboltable();

	String y = "yesterday";
	table.add_scope_level();                           
	System.out.println(table.level);                    //should output level 1

	table.add_variable_to_block("x" , table.level, 1);
	table.add_variable_to_block("y" , table.level, 2);
	table.add_variable_to_block("z" , table.level, 3);

	table.add_scope_level();                           //should output level 2
	System.out.println(table.level);

	table.add_variable_to_block("a" , table.level, 3);
	table.add_variable_to_block("b" , table.level, 4);
	table.add_variable_to_block("c" , table.level, 5);
	table.add_variable_to_block("w" , table.level, 6);
	table.add_variable_to_block("h" , table.level, 7);
	table.add_variable_to_block("r" , table.level, 8);
	table.add_variable_to_block("g" , table.level, 6);

	System.out.println(table.id_in_currentlevel("a"));  //will be true
	System.out.println("is variable x in scope? "+ table.id_in_scope("x") + "\n");			//will be true
	System.out.println("is variable a in scope? "+ table.id_in_scope("a") + "\n");			//will be true
	System.out.println("is variable m in scope? "+ table.id_in_scope("m") + "\n");         //will be false

	table.tableprint();
	table.remove_scope_level();
	table.tableprint();
	table.remove_scope_level();

	System.out.println (Integer.parseInt("2"));

	System.out.println(table.level);                    //will output level 0

	} */
}