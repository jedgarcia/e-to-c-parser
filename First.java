import java.io.*;
import java.util.List;
import java.util.ArrayList;

public class First{

	//Declarations for the first sets:
	private static List<String> BLOCK;
	private static List<String> STATEMENT_LIST;
	private static List<String> STATEMENT;
	private static List<String> ASSIGNMENT;
	private static List<String> REF_ID;
	private static List<String> ADDOP;
	private static List<String> MULTOP;
	private static List<String> EXPRESSION;
	private static List<String> TERM;

	//First() default constructor
	public First()
	{
		Init();		//initialize the ArrayList
	}

	//Checks to see if curtok (from parser) is a member 
	//of a the specified first set
	public static boolean Check(String name, String curtok)
	{
		//NO CASES FOR:
		//declaration_list; declaration; print; 
		//do; if; guarded_command; expr; term;
		//factor
		boolean answer=false;
		switch(name)
				{
				case "BLOCK":answer=Comp(BLOCK,curtok); break;
				case "STATEMENT_LIST": answer=Comp(STATEMENT_LIST,curtok); break;
				case "STATEMENT": answer=Comp(STATEMENT,curtok); break;
				case "ASSIGNMENT": answer=Comp(ASSIGNMENT,curtok); break;
				case "REF_ID": answer=Comp(REF_ID,curtok); break;
				case "ADDOP": answer=Comp(ADDOP,curtok); break;
				case "MULTOP": answer=Comp(MULTOP,curtok); break;
				case "EXPRESSION": answer=Comp(EXPRESSION,curtok); break;
				case "TERM": answer=Comp(TERM,curtok); break;
				default: break;
				}
		return answer;
	}

	//Compares curtok from parser (tok.kind) to 
	//the arraylist specified in the caller's argument
	private static boolean Comp(List<String> name, String curtok)
	{
		//by initializing the boolean value to false,
		//we can assume, in the loop, if there is a match
		//then curtok is in the first set of name.
		//Therefore, skip all future iterations of the loop, i.e., continue.
		boolean result=false;

		//traverses the specified list, as passed in the 
		//paramrter list
			for (int i=0; i<name.size(); i++)
					{
						if(name.get(i)==curtok)
							result=true;
							continue;				
					}
		return result;
	}

	//Initialize ArrayList (build the first sets):
	private static void Init()
	{
		//SINGLETON FIRST SETS NOT USED HERE:
		//declaration_list; declaration; print; 
		//do; if; guarded_command; expr; term;
		//factor

			BLOCK= new ArrayList<String>();
				BLOCK.add("TK.DECLARE");
				BLOCK.add("TK.TILDE");
				BLOCK.add("TK.NUM");
				BLOCK.add("TK.ID");
				BLOCK.add("TK.PRINT");
				BLOCK.add("TK.DO");
				BLOCK.add("TK.IF");
				BLOCK.add("TK.none");

			STATEMENT_LIST = new ArrayList<String>();
				STATEMENT_LIST.add("TK.TILDE");
			//	STATEMENT_LIST.add("TK.NUM");
				STATEMENT_LIST.add("TK.ID");
				STATEMENT_LIST.add("TK.PRINT");
				STATEMENT_LIST.add("TK.DO");
				STATEMENT_LIST.add("TK.IF");
				STATEMENT_LIST.add("TK.none");
				STATEMENT_LIST.add("TK.FOR");

			STATEMENT = new ArrayList<String>();
				STATEMENT.add("TK.TILDE");
				STATEMENT.add("TK.NUM");
				STATEMENT.add("TK.ID");
				STATEMENT.add("TK.PRINT");
				STATEMENT.add("TK.DO");
				STATEMENT.add("TK.IF");
				STATEMENT.add("TK.none");

			EXPRESSION = new ArrayList<String>();       // ADDED EXPRESSION
				EXPRESSION.add("TK.LPAREN");
				EXPRESSION.add("TK.RPAREN");
				EXPRESSION.add("TK.TILDE");
				EXPRESSION.add("TK.NUM");
				EXPRESSION.add("TK.ID");
				EXPRESSION.add("TK.PLUS");
				EXPRESSION.add("TK.MINUS");
				EXPRESSION.add("TK.TIMES");
				EXPRESSION.add("TK.DIVIDE");
				EXPRESSION.add("TK.none");

			TERM = new ArrayList<String>();         // ADDED TERM
				TERM.add("TK.LPAREN");
				TERM.add("TK.RPAREN");
				TERM.add("TK.TILDE");
				TERM.add("TK.NUM");
				TERM.add("TK.ID");
				TERM.add("TK.PLUS");
				TERM.add("TK.MINUS");
				TERM.add("TK.none");


			ASSIGNMENT = new ArrayList<String>();
				ASSIGNMENT.add("TK.TILDE");
				ASSIGNMENT.add("TK.NUM");
				ASSIGNMENT.add("TK.ID");
				ASSIGNMENT.add("TK.none");

			REF_ID = new ArrayList<String>();  
				REF_ID.add("TK.TILDE");
			//	REF_ID.add("TK.NUM");
				REF_ID.add("TK.ID");
				REF_ID.add("TK.none");

			ADDOP = new ArrayList<String>();
				ADDOP.add("TK.MINUS");
				ADDOP.add("TK.PLUS");
				ADDOP.add("TK.none");

			MULTOP = new ArrayList<String>();
				MULTOP.add("TK.TIMES");
				MULTOP.add("TK.DIVIDE");
				MULTOP.add("TK.none");
	}

/*
	//MAIN tests that everything works and outputs as it should! 
	 public static void main(String args[]) {
        
        new First();
        //First.Check("BLOCK");

        Token tok;
       	int linenumber=0;
      
       	//explicitely creates a token, for testing purposes
 		tok= new Token(TK.TILDE, new String(String.valueOf("~")), linenumber);
        System.out.println(First.Check("REF_ID",tok.kind.toString()));

       
         tok= new Token(TK.PLUS, new String(String.valueOf("+")), linenumber);
        System.out.println(First.Check("ADDOP",tok.kind.toString()));

        tok= new Token(TK.DIVIDE, new String(String.valueOf("/")), linenumber);
        System.out.println(First.Check("ADDOP",tok.kind.toString()));

 


    } */

}